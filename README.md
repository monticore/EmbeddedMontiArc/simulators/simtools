<!-- (c) https://github.com/MontiCore/monticore -->
This repo contains some useful tools for visualization of
[simulation server](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/server).

Python >=3.6 is required.

## Install
```
$ pip install -r requirements.txt
```

## Run
```
$ jupyter notebook
```
