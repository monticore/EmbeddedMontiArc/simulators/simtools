# (c) https://github.com/MontiCore/monticore  
import json
import math

from bs4 import BeautifulSoup
import networkx as nx


class Graph:
    def __init__(self, base_dir, map_name, num_sectors):
        self.base_path = f'{base_dir}/{map_name}'
        self.num_sectors = num_sectors

        self._complete_graph = None
        self._overlay = None

        self.osm_id_to_sector_idx_map = dict()
        self.cut_vertices = []
        self.cut_edges = []

        self._init()

    def _init(self):
        self.get_complete_graph()
        self.get_overlay()
        self.read_partition_result()

    def get_complete_graph(self):
        if not self._complete_graph:
            self._complete_graph = get_graph(f'{self.base_path}.osm')
        return self._complete_graph

    def get_sector(self, idx):
        sector = get_graph(self.get_sector_file_path(idx))
        to_remove = []
        for node in sector.nodes:
            sector_idx = self.osm_id_to_sector_idx_map[node]
            if sector_idx != idx:
                to_remove.append(node)

        for node in to_remove:
            sector.remove_node(node)
        return sector

    def get_overlay(self):
        if self._overlay:
            return self._overlay

        self._overlay = nx.drawing.nx_agraph.read_dot(f'{self.base_path}.part.{self.num_sectors}.overlay.dot')
        for node in self._overlay.nodes:
            # set lon/lat position
            nx.set_node_attributes(self._overlay, {node: self._complete_graph.nodes[node]})

        return self._overlay

    def get_sector_file_path(self, idx):
        return f'{self.base_path}.part.{self.num_sectors}-{idx}.osm'

    def read_partition_result(self):
        self.get_complete_graph()
        nodes = list(self._complete_graph.nodes)
        nodes.sort(key=lambda x: int(x), reverse=True)

        with open(f'{self.base_path}.part.{self.num_sectors}') as f:
            for line_no, line in enumerate(f):
                self.osm_id_to_sector_idx_map[nodes[line_no]] = int(line)

    def get_cut_nodes(self):
        return list(self._overlay.nodes)

    def get_cut_edges(self):
        return list(self._overlay.edges)

    def get_node_position(self, osm_id):
        if not isinstance(osm_id, str):
            osm_id = str(osm_id)
        return self._complete_graph.nodes[osm_id]['pos'][:2]


def get_graph(filename):
    G = nx.Graph()
    with open(filename, 'r') as f:
        osm = BeautifulSoup(f.read(), 'lxml')
    nodes = get_nodes(osm)

    for way in osm.find_all('way'):
        if not is_way(way):
            continue

        nds = way.find_all('nd')
        for i, nd in enumerate(nds):
            node = nodes[nd.get('ref')]
            G.add_node(node['id'], pos=(node['lon'], node['lat']), size=1)
            if i < len(nds) - 1:
                target = nodes[nds[i + 1].get('ref')]
                # t_lon = float(target.get('lon'))
                # t_lat = float(target.get('lat'))
                G.add_edge(node['id'], target.get('id'), distance=1)

    return G


def get_nodes(soup):
    nodes = dict()
    for n in soup.find_all('node'):
        nodes[n.get('id')] = {
            'id': n.get('id'),
            'lon': float(n.get('lon')),
            'lat': float(n.get('lat'))
        }

    return nodes


def is_way(soup):
    not_way = ["footway", "bridleway", "steps", "path", "cycleway", "pedestrian", "track", "elevator"]
    for tag in soup.find_all('tag'):
        if tag.get('k') == 'highway' and tag.get('v') not in not_way:
            return True
    return False


def get_bound(graph):
    min_lon = 1e10
    max_lon = -1e10
    min_lat = 1e10
    max_lat = -1e10

    for node in graph.nodes(data=True):
        lon = node[1]['pos'][0]
        lat = node[1]['pos'][1]

        if lon > max_lon:
            max_lon = lon
        elif lon < min_lon:
            min_lon = lon

        if lat > max_lat:
            max_lat = lat
        elif lat < min_lat:
            min_lat = lat

    return min_lon, min_lat, max_lon, max_lat


def main():
    g = Graph('/Users/ham/Projects/MontiSim/server/restful/src/test/resources/aachen6', 'Aachen', 6)
    g.get_overlay()
    assert  '60009520' in set(g.get_sector(4).nodes)
    print('hi')


if __name__ == "__main__":
    main()
